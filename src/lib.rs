pub(crate) use nalgebra::{DMatrix, DVector};
use snafu::prelude::*;

#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu(display("Gamma should positive, but it was '{gamma}'"))]
    GammaTooLow { gamma: f64 },
}

/// Gamma bus detection.
///
/// # Arguments
///
/// * `adj`: Adjacency matrix corresponding to a graph.
/// * `gamma`: Threshold factor for bus node degrees with respect to the median of the
///     (nonzero) remaining candidate node degrees.
///
/// # Returns
///
/// A pair of vectors containing the bus node indices and remaining non-bus node
/// indices, respectively.
///
/// # Errors
///
/// This function will return an error if gamma is too low.
pub fn gamma(adj: &DMatrix<f64>, gamma: f64) -> Result<(Vec<usize>, Vec<usize>), Error> {
    ensure!(gamma >= 0.0, GammaTooLowSnafu { gamma });

    let (rows, cols) = adj.shape();
    let dim = std::cmp::max(rows, cols);

    if dim <= 1 {
        return Ok((vec![], (0..dim).collect())); // Early return for trivial cases 0 and 1 node.
    }

    let (in_degree, out_degree) = get_degrees(&adj);
    let degrees = (in_degree + out_degree).data.as_vec().clone(); // Get regular vec.

    let mut nonbus: Vec<usize> = vec![];
    let mut candidates: Vec<usize> = vec![];
    let mut bus: Vec<usize> = vec![];

    for i in 0..dim {
        if degrees[i] > 0 {
            candidates.push(i);
        } else {
            nonbus.push(i);
        }
    }

    candidates.sort_by(|a, b| degrees[*a].cmp(&degrees[*b]));
    let mut degrees: Vec<f64> = (&candidates)
        .iter()
        .map(|&idx| degrees[idx] as f64)
        .collect();

    loop {
        let mut added = false;
        let mid = degrees.len() / 2;
        let median = degrees[mid];
        let threshold = gamma * (median as f64);
        while let (Some(degree), Some(idx)) = (degrees.pop(), candidates.pop()) {
            if degree < threshold {
                candidates.push(idx);
                degrees.push(degree);
                break;
            }
            bus.push(idx);
            added = true;
        }
        if !added {
            break;
        }
    }
    nonbus.extend(candidates);
    nonbus.sort(); // Return nonbus in original order.
    Ok((bus, nonbus))
}

/// Get the in and out degrees of a graph represented by its adjacency matrix.
///
/// # Example
/// ```
/// use nalgebra::{DMatrix, DVector};
/// use ratio_bus::get_degrees;
/// let adj = DMatrix::from_row_slice(3, 3, &[0.0, 5.0, 10.0, 1.0, 0.0, 0.0, 3.0, 10.0, 0.0]);
/// let (ins, outs) = get_degrees(&adj);
/// assert_eq!(ins, DVector::from_vec(vec![2, 1, 2]));
/// assert_eq!(outs, DVector::from_vec(vec![2, 2, 1]));
/// ```
pub fn get_degrees(adj: &DMatrix<f64>) -> (DVector<usize>, DVector<usize>) {
    let (rows, cols) = adj.shape();
    let mut interfaces = DMatrix::<usize>::zeros(rows, cols);
    let mut in_degree = DVector::<usize>::zeros(rows);
    let mut out_degree = DVector::<usize>::zeros(cols);
    for i in 0..rows {
        for j in 0..cols {
            if adj[(i, j)] <= 0.0 {
                continue;
            }
            interfaces[(i, j)] = 1;
            in_degree[(i)] = in_degree[(i)] + 1;
            out_degree[(j)] = out_degree[(j)] + 1;
        }
    }
    (in_degree, out_degree)
}

#[cfg(test)]
#[cfg(not(tarpaulin_include))]
mod tests {
    use super::*;
    #[allow(unused_imports)]
    use pretty_assertions::{assert_eq, assert_ne, assert_str_eq};

    #[test]
    fn example() {
        let adj = DMatrix::from_row_slice(
            4,
            4,
            &[
                0.0, 5.0, 10.0, 0.0, 1.0, 0.0, 0.0, 0.0, 3.0, 10.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
            ],
        );
        let (ins, outs) = get_degrees(&adj);
        assert_eq!(
            ins,
            DVector::from_vec(vec![2, 1, 2, 0]),
            "In degrees should match."
        );
        assert_eq!(
            outs,
            DVector::from_vec(vec![2, 2, 1, 0]),
            "Out degrees should match."
        );

        let (bus, nonbus) = gamma(&adj, 100.0).unwrap();
        assert_eq!(bus, vec![], "Bus should be empty for gamma=100.");
        assert_eq!(
            nonbus,
            vec![0, 1, 2, 3],
            "Nonbus should be full for gamma=100."
        );

        let (bus, nonbus) = gamma(&adj, 1.2).unwrap();
        assert_eq!(bus, vec![0], "Bus should contain 0 for gamma=1.2.");
        assert_eq!(
            nonbus,
            vec![1, 2, 3],
            "Nonbus should be [1,2] for gamma=100."
        );
    }

    #[test]
    fn edge_cases() {
        let single = DMatrix::from_row_slice(1, 1, &[1.0]);
        let result = gamma(&single, -1.0);
        result.unwrap_err();
        assert_eq!(gamma(&single, 1.0).unwrap(), (vec![], vec![0]));
    }
}
