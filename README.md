# Ratio bus detection algorithms and heuristics.

Bus detection is the detection of integrative components within a graph or network
structure. The naming is derived from "bus" components in computing and electronics,
which are the components that most others are plugged into to distribute power or
communication.

# Changelog

## [0.1.0] - 2022-10-10

### Added

- Basic gamma bus detection.
